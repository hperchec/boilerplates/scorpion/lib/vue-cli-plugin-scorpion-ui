# Scorpion UI plugin for vue-cli

> This plugin is automatically used in the preset: [hperchec/boilerplates/scorpion/lib/vue-cli-preset-scorpion-ui](https://gitlab.com/hperchec/boilerplates/scorpion/lib/vue-cli-preset-scorpion-ui)

Requires **@vue/cli v5.0.8**.

## Development

To test locally, run the following command:

```bash
# TODO
# ...
# See also package.json scripts
# for further informations
```

----

Made with ❤ by [Hervé Perchec](http://herve-perchec.com/)