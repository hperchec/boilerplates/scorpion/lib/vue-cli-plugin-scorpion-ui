module.exports = [
  {
    name: 'defaultTemplate',
    type: 'confirm',
    message: 'Use Scorpion UI default template?',
    description: `The default template will be automatically used`
  },
  {
    name: 'projectType',
    type: 'list',
    message: 'If your package.json file has "type": "module" property set, use "module"',
    description: `Choose the project type`,
    choices: [
      {
        name: 'module',
        value: 'module',
        description: 'See juisy CLI doc'
      },
      {
        name: 'commonjs',
        value: 'commonjs',
        description: 'See juisy CLI doc'
      }
    ]
  }
]