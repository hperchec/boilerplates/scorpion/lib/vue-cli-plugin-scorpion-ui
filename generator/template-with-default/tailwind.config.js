// Tailwind utils
const { genConfig } = require('@hperchec/scorpion-ui-template-default/shared/tailwind/config')

// Globals
const GLOBALS = require('./globals.config.json')

// genConfig options
const options = {
  globals: GLOBALS,
  themePrefix: '@',
  generateColorVariants: true,
  flattenColors: true
}

// Tailwind config
const config = {
  content: [
    './public/index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}',
    'node_modules/vue-tailwind/dist/*.js',
    'node_modules/@hperchec/scorpion-ui-template-default/dist/*.js'
  ],
  theme: {
    screens: {
      ...Object.keys(GLOBALS.BREAKPOINTS).reduce((breakpoints, identifier) => {
        // Transform identifier to lowercase (ex: '2XL' => '2xl')
        breakpoints[identifier.toLowerCase()] = `${GLOBALS.BREAKPOINTS[identifier]}px`
        return breakpoints
      }, {})
    }
  }
}

module.exports = genConfig(config, options)
