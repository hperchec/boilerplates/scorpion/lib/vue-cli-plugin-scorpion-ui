import { Core, toolbox } from '@hperchec/scorpion-ui'

import App from './App'
import layouts from './layouts'
import commons from './commons'
import publicComponents from './public'
import privateComponents from './private'
import './vendor/vue-awesome'

const { mergeContext } = toolbox.template.utils

// Override App component
Core.context.vue.components.App = App
Core.context.vue.components.layouts.public = layouts.public
Core.context.vue.components.layouts.private = layouts.private
delete Core.context.vue.components.commons.VButton
mergeContext(Core.context.vue.components, 'commons', commons)
mergeContext(Core.context.vue.components, 'public', publicComponents)
mergeContext(Core.context.vue.components, 'private', privateComponents)
