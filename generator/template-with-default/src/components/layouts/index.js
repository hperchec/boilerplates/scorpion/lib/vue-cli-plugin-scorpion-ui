import publicLayout from './public'
import privateLayout from './private'

export default {
  public: publicLayout,
  private: privateLayout
}
