import { Core } from '@hperchec/scorpion-ui'

const VariantableOptions = Core.context.vue.mixins.variantable.VariantableOptions
const VButtonStyle = Core.context.vue.components.commons.VButton.style

/* eslint quote-props: ["error", "consistent"] */

export default VariantableOptions.extend(VButtonStyle, (inheritsFromVariant, createVariantExtensions) => ({
  fixedClasses: [
    // ...
  ],
  classes: [
    // ...
  ],
  variants: {
    'coffee': [
      'border-transparent',
      'px-3',
      'py-1',
      'leading-normal',
      'hover:shadow',
      'focus:shadow',
      'disabled:text-gray-400',
      '@light:bg-@light-coffee',
      '@light:text-white',
      '@light:hover:bg-@light-coffee-400',
      '@dark:bg-@dark-coffee',
      '@dark:text-white',
      '@dark:hover:bg-@dark-coffee-400'
    ],
    'border-coffee': [
      '@light:border-@light-coffee',
      '@dark:border-@dark-coffee',
      'px-3',
      'py-1',
      'leading-normal',
      'hover:shadow',
      'focus:shadow',
      'disabled:text-gray-400',
      'bg-transparent',
      '@light:text-@light-coffee',
      '@dark:text-@dark-coffee-200'
    ]
  }
}))
