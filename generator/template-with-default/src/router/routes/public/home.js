import { Core } from '@hperchec/scorpion-ui'

export default {
  ...Core.context.services.router.routes.public.Home
}
