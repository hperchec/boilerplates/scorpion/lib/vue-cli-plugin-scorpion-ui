import { Core, toolbox } from '@hperchec/scorpion-ui'

import routes from './routes'

/* eslint-disable dot-notation */

const { mergeContext } = toolbox.template.utils

// Remove default public "Home" route
delete Core.context.services['router'].routes.public.Home
// Merge routes
mergeContext(Core.context.services['router'], 'routes', routes)
