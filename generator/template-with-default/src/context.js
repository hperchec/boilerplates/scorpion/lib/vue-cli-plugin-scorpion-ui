// Translations
import '@/i18n'
// Listeners
import '@/listeners'
// Router
import '@/router'
// Components
import '@/components'

/**
 * Vue plugins
 */

// Core.registerVuePlugin('vue-plugin', VuePlugin, { ... })
