const { execSync } = require('child_process')
const fs = require('fs')

module.exports = (api, options = {}, rootOptions = {}) => {
  console.log() // blank line
  // First, install @hperchec/juisy
  console.log('🍊  Installing juisy...')
  execSync('npm install -D @hperchec/juisy', { cwd: api.resolve('./'), stdio: 'pipe' })
  execSync(`npx juisy squeeze --type=${options.projectType}`, { cwd: api.resolve('./'), stdio: 'inherit' })
  // Save added script, else vue-cli will overwrite it
  const addedScripts = require(api.resolve('./package.json')).scripts

  // Extend package.json
  api.extendPackage({
    dependencies: {
      // ...
    },
    devDependencies: {
      'eslint': '8.51.0',
      '@vue/eslint-config-standard': '^8.0.1',
      'eslint-plugin-n': '^16.1.0',
      'eslint-plugin-node': '^11.1.0',
      'eslint-plugin-standard': '^4.1.0',
      'eslint-plugin-vue': '^9.21.1',
      'eslint-webpack-plugin': '^4.2.0',
      '@intlify/eslint-plugin-vue-i18n': '^2.0.0',
      'serve': '^14.2.0'
    },
    scripts: {
      dev: 'npm run serve',
      ...addedScripts,
      // We disable postinstall script injected by @hperchec/juisy
      // because vue-cli runs installation with --legacy-peer-deps option
      // so it will throw an error if postinstall script runs
      postinstall: undefined
    },
    postcss: { // creates postcss.config.js file
      plugins: {
        autoprefixer: {}
      }
    },
    babel: {
      plugins: [
        '@babel/plugin-transform-class-static-block'
      ]
    },
    eslintConfig: {
      globals: {
        __GLOBALS__: false,
        // Root app instance
        __ROOT_VUE_INSTANCE__: false,
        // App version
        __APP_VERSION__: false,
        // App name
        __APP_NAME__: false
      },
      rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'array-bracket-spacing': [
          'error',
          'always'
        ],
        'vue/multi-word-component-names': 'off',
        '@intlify/vue-i18n/no-raw-text': [
          'error',
          {
            ignorePattern: '^[-+#:()|&"\'.!?*]+$'
          }
        ],
        '@intlify/vue-i18n/no-html-messages': 'error',
        '@intlify/vue-i18n/no-missing-keys': 'off',
        '@intlify/vue-i18n/no-v-html': 'warn'
      },
      extends: [
        // @vue standard config
        '@vue/standard',
        // eslint-plugin-vue
        'plugin:vue/essential',
        'plugin:vue/strongly-recommended',
        'plugin:vue/recommended',
        // @intlify/eslint-plugin-vue-i18n
        'plugin:@intlify/vue-i18n/recommended'
      ],
      root: true,
      env: {
        node: true
      },
      parserOptions: {
        parser: '@babel/eslint-parser'
      },
      settings: {
        // eslint-plugin-vue-i18n
        // See also: https://eslint-plugin-vue-i18n.intlify.dev/started.html#settings-vue-i18n
        'vue-i18n': {
          localeDir: [
            {
              // Translations from Scorpion-UI library
              // See also: https://eslint-plugin-vue-i18n.intlify.dev/started.html#configuration-eslintrc
              pattern: './node_modules/@hperchec/scorpion-ui/src/services/i18n/messages/**/*.json',
              localePattern: /^.*\/(?<locale>[A-Za-z0-9-_]+)\/.*\.json$/,
              localeKey: 'path'
            },
            {
              // Our own translations
              pattern: './src/i18n/**/*.json', // extention is glob formatting!
              localePattern: /^.*\/(?<locale>[A-Za-z0-9-_]+)\/.*\.json$/,
              localeKey: 'path'
            }
          ]
        }
      }
    }
  })

  // Render template
  api.render('./template', {})

  // If we use scorpion-ui-template-default
  if (options.defaultTemplate) {
    // Add dependency
    api.extendPackage({
      postcss: {
        plugins: {
          'postcss-import': {},
          autoprefixer: {},
          tailwindcss: {
            config: './tailwind.config.js'
          }
        }
      },
      eslintConfig: {
        settings: {
          'vue-i18n': {
            localeDir: [
              {
                // Translations from Scorpion-UI default template
                pattern: './node_modules/@hperchec/scorpion-ui-template-default/src/i18n/**/*.json',
                localePattern: /^.*\/(?<locale>[A-Za-z0-9-_]+)\/.*\.json$/,
                localeKey: 'path'
              }
            ]
          }
        }
      }
    })
    // Render template
    api.render('./template-with-default', {})
  }

  // On createComplete hook
  api.onCreateComplete(function () {
    // Remove obsolete dependencies
    execSync(`npm uninstall eslint-plugin-promise`, { cwd: api.resolve('./'), stdio: 'pipe' })
    // Re-enable the postinstall script injected by @hperchec/juisy
    execSync(`npm install -D @hperchec/juisy`, { cwd: api.resolve('./'), stdio: 'pipe' })
    execSync(`npx juisy squeeze`, { cwd: api.resolve('./'), stdio: 'pipe' })

    // Install manually scorpion-ui because
    // using api.extendPackage({ dependencies: '@hperchec/scorpion-ui' })
    // will not install peerDependencies
    // TO DO: find a workaround ? it seems that vue-cli call `npm install` command with `--legacy-peer-deps` option for npm > v7...
    console.log()
    console.log('🦂  Installing scorpion-ui...')
    execSync('npm install @hperchec/scorpion-ui@rc', { cwd: api.resolve('./'), stdio: 'pipe' })
    console.log() // blank line

    // If we use scorpion-ui-template-default, same as scorpion-ui
    if (options.defaultTemplate) {
      console.log('🦂  Installing scorpion-ui-template-default...')
      execSync('npm install @hperchec/scorpion-ui-template-default@rc', { cwd: api.resolve('./'), stdio: 'pipe' })
      console.log() // blank line
    }

    // Final touches
    console.log('💅  Final touches...')

    // Remove ./src/App.vue auto-generated file
    const filePath = api.resolve('src/App.vue')
    if (fs.existsSync(filePath)) {
      fs.unlinkSync(filePath)
    }
    console.log() // blank line
  })
}