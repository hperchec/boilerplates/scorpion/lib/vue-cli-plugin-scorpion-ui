const path = require('path')
const createConfig = require('@hperchec/scorpion-ui/shared/vue/config/create-config')

// Get aliases
const aliases = require('./build/aliases')
// Get project package.json file
const packageJson = require('./package.json')
// Get project globals
const globals = require('./globals.config.json')
// Project root path
const rootPath = path.resolve(__dirname)
// Create config tool options
const options = {
  /* ... */
}

// Custom config
const config = {
  devServer: {
    // ...
  },
  configureWebpack: {
    resolve: {
      alias: aliases,
      fallback: {
        crypto: false
      }
    }
  },
  chainWebpack: _config => {
    // ...
  },
  css: {
    loaderOptions: {
      postcss: {
        postcssOptions: {
          config: path.resolve(rootPath, 'postcss.config.js')
        }
      }
    }
  },
  /**
   * See @vue/cli-plugin-pwa: https://cli.vuejs.org/core-plugins/pwa.html#configuration
   */
  pwa: {
    /**
     * Default: "name" field in package.json
     *
     * Used as the value for the apple-mobile-web-app-title meta tag in the generated HTML. Note you will need to edit public/manifest.json to match this.
     */
    // name: '',
    /**
     * Theme color
     *
     * Default: '#4DBA87'
     */
    themeColor: '#4DBA87',
    /**
     * Microsoft tile color
     *
     * Default: '#000000'
     */
    msTileColor: '#000000',
    /**
     * Default: 'manifest.json'
     *
     * The path of app’s manifest. If the path is an URL, the plugin won't
     * generate a manifest.json in the dist directory during the build.
     */
    manifestPath: 'manifest.json',
    /**
     * Default: {}
     *
     * The object will be used to generate the manifest.json
     *
     * If the following attributes are not defined in the object, the options of pwa or default options will be used instead.
     *     name: pwa.name
     *     short_name: pwa.name
     *     start_url: '.'
     *     display: 'standalone'
     *     theme_color: pwa.themeColor
     */
    manifestOptions: {},
    /**
     * Defaults:
     *   {
     *     faviconSVG: 'img/icons/favicon.svg',
     *     favicon32: 'img/icons/favicon-32x32.png',
     *     favicon16: 'img/icons/favicon-16x16.png',
     *     appleTouchIcon: 'img/icons/apple-touch-icon-152x152.png',
     *     maskIcon: 'img/icons/safari-pinned-tab.svg',
     *     msTileImage: 'img/icons/msapplication-icon-144x144.png'
     *   }
     */
    iconPaths: {
      faviconSVG: 'img/icons/favicon.svg',
      favicon32: 'img/icons/favicon-32x32.png',
      favicon16: 'img/icons/favicon-16x16.png',
      appleTouchIcon: 'img/icons/apple-touch-icon-152x152.png',
      maskIcon: 'img/icons/safari-pinned-tab.svg',
      msTileImage: 'img/icons/msapplication-icon-144x144.png'
    }
  }
}

// Create vue config
module.exports = createConfig(packageJson, globals, rootPath, config, options)
