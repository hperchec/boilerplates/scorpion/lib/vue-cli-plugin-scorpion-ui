import { Core, toolbox } from '@hperchec/scorpion-ui'

// Import main App internationalization
import en from './en'
import fr from './fr'

const { mergeContext } = toolbox.template.utils

// Messages
const messages = {
  en,
  fr
}

/* eslint-disable dot-notation */
mergeContext(Core.context.services['i18n'], 'messages', messages)

export default messages
