/*
 * Init app script
 */

// Vue
import Vue from 'vue'
// Core
import { Core } from '@hperchec/scorpion-ui'

/**
 * Configure Vue
 */
Vue.config.productionTip = false

/**
 * First, initialize
 */

// Core.onInitialized(() => {})

Core.init(Vue)
