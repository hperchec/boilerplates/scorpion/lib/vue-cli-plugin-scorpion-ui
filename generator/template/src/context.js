// Translations
import '@/i18n'
// Listeners
import '@/listeners'
// Components
import '@/components'

/**
 * Vue plugins
 */

// Core.registerVuePlugin('vue-plugin', VuePlugin, { ... })
