import { Core, toolbox } from '@hperchec/scorpion-ui'

import App from './App'
import commons from './commons'
// Routes components
import HomeIndex from './public/views/home/Index.vue'

const { mergeContext } = toolbox.template.utils

// Override App component
Core.context.vue.components.App = App
// Merge global components
mergeContext(Core.context.vue.components, 'commons', commons)
// Routes components
Core.context.vue.components.public.views.home.Index = HomeIndex

/**
 * Vue global components
 */

// const Vue = Core.Vue
// ...
