/**
 * Common components
 */

import { utils } from '@hperchec/scorpion-ui'

const { isDef } = utils

const requireCommonComponents = require.context('@/components/commons', true, /.*\.vue$/)
const commonComponents = {}

// Loop on common components
requireCommonComponents.keys().forEach(fileName => {
  // Get component config
  const componentConfig = requireCommonComponents(fileName)
  // Get name of component
  const componentName = componentConfig.default.name
  // Check if component has "name" property defined
  if (!isDef(componentName)) {
    console.warn(`Error during "commons" components registration: component "${fileName}" doesn't have "name" property defined. Please give a name to the component.`)
  } else {
    // Check if component has a name already taken
    if (isDef(commonComponents[componentName])) {
      console.warn(`Error during "commons" components registration: component name "${componentName}" already taken. Please provide an available name.`)
    } else {
      // All the component was exported with `export default`
      commonComponents[componentName] = componentConfig.default
    }
  }
})

export default commonComponents