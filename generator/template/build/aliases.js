const path = require('path')
// Project root path
const rootPath = path.resolve(__dirname, '../')
// Resolve from root path
const resolve = (...args) => { // eslint-disable-line no-unused-vars
  return path.join(rootPath, ...args)
}

/**
 * Aliases for webpack
 */
module.exports = {
  // Add your own aliases
  // '@hperchec/scorpion-ui$': '@hperchec/scorpion-ui/dist/scorpion-ui.js'
}
