module.exports = {
  extends: [
    'stylelint-config-standard',
    'stylelint-config-standard-vue'
  ],
  rules: {
    'at-rule-empty-line-before': [ 'always', {
      except: [ 'blockless-after-same-name-blockless', 'first-nested', 'inside-block' ],
      ignore: [ 'after-comment' ]
    } ],
    'declaration-empty-line-before': null,
    'at-rule-no-unknown': null,
    'selector-id-pattern': [
      // See: https://github.com/stylelint/stylelint-config-standard/blob/08b6e948de94189c1d3ce376e50f2c42dc94580e/index.js#L102
      // We add possible underscore '_' as first character for custom naming
      '^[_]?([a-z][a-z0-9]*)(-[a-z0-9]+)*$',
      {
        message: (selector) => `Expected id selector "${selector}" to be kebab-case`
      }
    ]
  }
}
